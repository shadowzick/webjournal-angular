<?php

class Profile extends \Eloquent {
	protected $fillable = ['user_id', 'profile_img', 'quote'];

	public function user()
	{
	   return $this->belongsTo('User', 'user_id');
	}
}