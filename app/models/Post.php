<?php

class Post extends \Eloquent {
	protected $fillable = ['title', 'body', 'user_id', 'vision'];

	public function user(){
	   return $this->belongsTo('User', 'user_id');
	}

}
