<?php

class UsersTableSeeder extends Seeder {

    public function run()
    {
            User::create([
                'username'  => 'shadowzick',
                'email'     => 'shadowzick@yahoo.com',
                'firstname' => 'David',
                'lastname'  => 'Lopena',
                'password'  => 'password' //Hash attribute has already been set on model
            ]);

            User::create([
                'username'  => 'shadowzick01',
                'email'     => 'shadowzick01@yahoo.com',
                'firstname' => 'David01',
                'lastname'  => 'Lopena01',
                'password'  => 'password' //Hash attribute has already been set on model
            ]);
    }

}