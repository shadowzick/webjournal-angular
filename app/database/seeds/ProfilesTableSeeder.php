<?php

class ProfilesTableSeeder extends Seeder {

	public function run()
	{
		Profile::create([
			'user_id'	=> 1,
			'profile_img'=> '/users/images/david-cartoonize.png',
			'quote' => 'Who the Son sets free is free indeed!'
		]);
	}

}