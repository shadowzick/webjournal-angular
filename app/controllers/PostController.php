<?php

class PostController extends \BaseController
{

    public function __construct()
    {
        $this->beforeFilter('auth');
    }

    /**
     * Display a listing of the resource.
     * GET /post
     *
     * @return Response
     */
    public function getAllByUser($username)
    {
//		$posts = Post::where('user_id', Session::get('user_id'))->orderBy('id' ,'desc')->get();
//		The frontend handles the orderby
        $posts = User::where('username', $username)->with('posts')->get();
        if ($posts->count()) {
            return Response::json($posts, 200);
        } else {
            return Response::json(array('message', 'This user has no post'), 400);
        }
//		Use this query pattern on the homepage
//		$posts = User::find(Session::get('user_id'))->with(array('posts' => function($post)
//		{
//			$post->orderBy('id', 'desc');
//		}))->get();
//        $user = User::find(Session::get('user_id'));
//		Take not of this convention (postfirst, thenuser) so you wont break the frontend
    }

    /**
     * Show the form for creating a new resource.
     * GET /post/create
     *
     * @return Response
     */
    public function create()
    {
        $rules = array(
//            'title' => 'required',
            'body' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Response::json(['message' => $validator->messages()], 406);
        } else {
            $newPost = Post::create([
                'user_id' => Session::get('user_id'),
                'title' => Input::get('title'),
                'body' => Input::get('body')
            ]);

            return Response::json([$newPost, 'message' => 'Post has been successfully created!'], 200);
        }
    }

    /**
     * Display the specified resource.
     * GET /post/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        if ($id) {
            $post = Post::find($id);
            $user = Post::find($id)->user;
            return Response::json(array($post, $user), 200);
        } else {
            return Response::json(array('message' => 'Post id was not supplied', 401));
        }
    }


    /**
     * Update the specified resource in storage.
     * PUT /post/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $user = Post::find($id)->user;
        if($user->id == Session::get('user_id')) {
            $post = Post::find($id);
            $post->title = Input::get('title');
            $post->body = Input::get('body');
            $post->save();
            return Response::json(array($post, 'message' => 'Post was successfully updated.'), '200');
        } else {
            return Response::json(array('message' => "You don't have any permission to edit this post.", 401));
        }
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /post/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Post::destroy($id);
        return Response::json(array('message', 'Post has been successfully deleted', 200));
    }

}