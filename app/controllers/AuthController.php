<?php

class AuthController extends \BaseController {


    public function postLogin()
    {
        $rules = array(
            'username' => 'required',
            'password' => 'required'
        ); 

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()) {
            return Response::json(['message' => $validator->messages()]);
        } else {
            $data = array(
                'username' => Input::get('username'),
                'password' => Input::get('password')
            );

            if(Auth::attempt($data, true)) {
                $user = User::find(Auth::user()->id);
                $user->token = Str::random(20);
                $user->save();

                $response = array(
                    'success' => true,
                    'message' => 'You are now logged in!',
                    'token' => $user->token
                );

                Session::put('user_id', $user->id);

                return Response::json(array($response, $user, 'token' => $user->token), 200);
            } else {
                $response = array(
                    'success' => false,
                    'message' => 'Invalid username/password.'
                );
                return Response::json($response, 401);
            }
        } 
    }

    public function logout()
    {
        $user = User::find(Auth::user()->id);
        $user->token = '';

        $response = array(
            'success' => true,  
            'message' => 'You have successfully logged out.'
        );
        Auth::logout();
        Session::flush();
        return Response::json($response, 200);
    }

    public function signup()
    {
        $rules = array(
            'firstname' => 'required|alpha',
            'lastname'  => 'required|alpha',
            'username'  => 'required|alpha_num|unique:users',
            'password'  => 'required',
            'email'     => 'unique:users|required'
        );

        $data = Input::all(); 

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Response::json(['message' => $validator->messages()]);
        } else {
            User::create($data);
            return Response::json('You have successfully registered!');

        }

    }
}