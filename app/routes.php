<?php

Route::get('/', function()
{
	return View::make('index');
});


Route::group(array('prefix' => 'auth', 'before' => 'csrf'), function()
{
        Route::post('/login', 'AuthController@postLogin');
        Route::post('/logout', 'AuthController@logout');
        Route::post('/signup', 'AuthController@signup');
});

Route::group(array('prefix' => 'api', 'before' => 'auth'), function()
{

//    User
    Route::get('user', 'UserController@getLoggedInUserData');
//   POST
    Route::get('post/{username}', 'PostController@getAllByUser');
    Route::get('post/{id}', 'PostController@show');
//    Route::resource('post', 'PostController'); //There are some errors here maybe because of the API prefix

//    Route::group(array('before' => 'csrf'), function()
//    {
        Route::post('post/create', array('uses' => 'PostController@create'));
        Route::put('post/{id}', array('uses' => 'PostController@update'));
//    });

//    PROFILE
    Route::get('profile/{username}', 'ProfileController@getByUserName');

});




