app.filter('startFrom', function() {
    return function(input, start) {
        start = +start;
        if(angular.isArray(input) && input.length > 0) {
            return input.slice(start);
        }
        return input;
    }
});