app.controller('myjournalController', function($scope, $http, $location, Post, CSRF_TOKEN, $sanitize, Profile, $stateParams, $rootScope) {
        $scope.newPost = {};
        var defaultTitle = 'Click here to add title';
        $scope.newPost.title = defaultTitle;
        $scope.posts = {};
        $scope.currentPage = 0;
        $scope.pageSize = 3;

        window.csrf_token = CSRF_TOKEN;

        Profile.getByUserName($stateParams.username).then(function(res) {
            if (res) {
                $scope.user = res.data[0];
                $scope.profile = res.data[0].profile;
            }
        }, function(res) {
            $location.path('/');
            Shadowzick.notify(res.data.message, 'warning');
        });

        Post.getAllByUser($stateParams.username).then(function(res){
            $scope.posts = res.data[0].posts;
            $scope.author = res.data[0];
        });


        var updatePosts = function(newPost) {
            $scope.posts.push(newPost);
        }

        var sanitizeCredentials = function(credentials) {
            return {
                title: $sanitize(credentials.title),
                body : $sanitize(credentials.body),
                _token: CSRF_TOKEN
            }
        }

        $scope.create = function() {
            if ($scope.newPost.title === defaultTitle) {
                $scope.newPost.title = '';
            }

            Post.create(sanitizeCredentials($scope.newPost)).then(function(res){

                $scope.posts.splice(0, 0, res.data[0]);
                $scope.three = $scope.three + 1;
                $scope.newPost.title = '';
                $scope.newPost.body = '';
                if ($scope.currentPage != 0) {
                    $scope.currentPage = 0;
                }
                Shadowzick.notify('Successfully posted.', 'info')
            }, function(res){
                var err = res.data.message;
                for (var i in err) {
                    if (err.hasOwnProperty(i)) {
                        var err_message = JSON.stringify(err[i]).replace(/[\]"")}[{(]/g, '');
                        Shadowzick.notify(err_message, 'warning');
                    }
                }

            });
        }
    });


