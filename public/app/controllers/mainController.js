    app.controller('mainController', function($scope, $http, $location, $auth, User) {
        // create a message to display in our view
        $scope.subtitle = 'Subtitle';
        $scope.message = 'Everyone come and see how good I look!';

        $scope.logout = function() {
            $auth.logout()
                .then(function(data){
                    $location.path('/login');
                    Shadowzick.notify('You have logged out.', 'info');
                });
        }

        User.getLoggedInUser().then(function(res) {
          $scope.user = res.data;
        });

        // Redirects
        $scope.redirect = {};

        $scope.redirect.home = function() {
            $location.path('/');
        }

        $scope.redirect.myjournal = function() {
            $location.path('/myjournal');
        }

        $scope.redirect.signup = function() {
            $location.path('/signup');
        }

    });
 