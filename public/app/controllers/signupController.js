    app.controller('signupController', function($scope, $http, $location, Auth, CSRF_TOKEN) {

        $scope.signup = function() {
            $scope.userData._token = CSRF_TOKEN;
            Auth.signup($scope.userData)
                .then(function(xhr){
                    $location.path('/login');
                    Shadowzick.notify('You have successfully registered!', 'info');
                }, function(xhr){
                    console.log('Error', xhr);
                });
        }
    });



