    app.controller('loginController', function($scope, $http, $location, Auth, $auth, CSRF_TOKEN) {
        $scope.login = function() {

            $scope.loginData._token = CSRF_TOKEN;
            $auth.login($scope.loginData)
                .then(function(xhr) {
                    // Success handler
                    $location.path('/');
                    Shadowzick.notify(xhr.data[0].message, 'info');
                }, function(xhr) {
                    // Error handler
                    $location.path('/login');
                    Shadowzick.notify(xhr.data.message, 'warning');
                });
        }

    });

