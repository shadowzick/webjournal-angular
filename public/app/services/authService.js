angular.module('authService', [])
    .factory('Auth', function($http) {
        return {
            login: function(loginData) {

                return $http
                    .post(PUBLIC_URL + '/auth/login', loginData);

            },

            logout: function() {
                return $http
                    .post(PUBLIC_URL + '/auth/logout');
            },

            signup: function(userData) {
                return $http
                    .post(PUBLIC_URL +'/auth/signup', userData);
            }
        }
    });