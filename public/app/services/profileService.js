angular.module('profileService', [])
    .factory('Profile', function($http) {
        return {
            getByUserName: function(username) {
                return $http
                    .get(API_URL + '/profile/' + username);
            }
        }
    });