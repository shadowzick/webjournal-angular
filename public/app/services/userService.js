angular.module('userService', [])
    .factory('User', function($http) {
        return {
            getLoggedInUser: function() {
                return $http.get(API_URL + '/user');
            }

        }
    });