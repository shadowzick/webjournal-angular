angular.module('postService', [])
    .factory('Post', function($http) {
        return {
            getAllByUser: function(username) {

                return $http.get(API_URL + '/post/' + username);

            },

            create: function(newPost) {
                return $http.post(API_URL + '/post/create', newPost);

            }



        }
    });