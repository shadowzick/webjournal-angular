
    // create the module and name it app
        // also include ngRoute for all our routing needs
    var app = angular.module('app', ['authService', 'contenteditable', 'postService', 'userService', 'ngRoute', 'ui.router', 'ngSanitize', 'satellizer', 'ngAnimate', 'profileService' ]);

    var VIEW_URL = PUBLIC_URL + '/app';

    // configure our routes
    app.config(function($stateProvider, $authProvider, $urlRouterProvider) {
        $stateProvider


            .state('/', {
                url         : '/',
                templateUrl : VIEW_URL + '/views/home.php',
                controller  : 'mainController',
                resolve     : {
                    authenticated: function($location, $auth) {
                        if (!$auth.isAuthenticated()) {
                            return $location.path('/login');
                        }
                    }
                }
            })

            .state('myjournal', {
                url         : '/myjournal/:username',
                templateUrl : VIEW_URL + '/views/myjournal.php',
                controller  : 'myjournalController',
                resolve     : {
                    authenticated: function($location, $auth) {
                        if (!$auth.isAuthenticated()) {
                            return $location.path('/login');
                        }
                    }
                }
            })

            .state('post', {
                url         : '/post',
                templateUrl : VIEW_URL + '/views/individual-post.php',
                controller  : 'individual-postController',
                resolve     : {
                    authenticated: function($location, $auth) {
                        if (!$auth.isAuthenticated()) {
                            return $location.path('/login');
                        }
                    }
                }
            })

            .state('login', {
                url         : '/login',
                templateUrl : VIEW_URL + '/views/login.html',
                controller  : 'loginController',
                resolve     : {
                    authenticated: function($location, $auth) {
                        if ($auth.isAuthenticated()) {
                            return $location.path('/myjournal');
                        }
                    }
                }
            })

            .state('signup', {
                url         : '/signup',
                templateUrl : VIEW_URL + '/views/signup.html',
                controller  : 'signupController',
                resolve     : {
                    authenticated: function($location, $auth) {
                        if ($auth.isAuthenticated()) {
                            return $location.path('/myjournal');
                        }
                    }
                }
            })
    });

