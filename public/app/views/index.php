<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
    <meta charset="UTF-8">
    <title>Web Journal | Your Online Journal</title>
    <?php echo HTML::style('css/bootstrap.min.css'); ?>
    <?php echo HTML::style('css/socialbox.css'); ?>
    <?php echo HTML::style('css/style.css'); ?>
    <?php echo HTML::style('css/post.css'); ?>

  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
    <?php echo HTML::style('css/animate.css'); ?>
    <script>
      var PUBLIC_URL = "<?php echo URL::to(''); ?>";
      var API_URL = "<?php echo URL::to('/api'); ?>";
    </script>

  <style>
    body {
      background: url('/users/images/default.jpg') no-repeat center center fixed;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
    }
  </style>
</head>

  <!-- define angular controller -->
  <body ng-controller="mainController">

<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">WebJournal</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a ng-href="#/" >Home</a></li>
        <li><a ng-href="#/myjournal/{{user.username}}" >My Journals</a></li>
      </ul>
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="account.html">Account</a></li>
            <li class="divider"></li>
            <li><a href="#" ng-click="logout()">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>



  <!-- MAIN CONTENT AND INJECTED VIEWS -->
    <div ui-view></div>

  </div>


  <?php echo HTML::script('js/jquery.min.js'); ?>
  <?php echo HTML::script('js/angular.min.js'); ?>
  <?php echo HTML::script('js/angular-routes.min.js'); ?>
  <?php echo HTML::script('js/angular-sanitize.min.js'); ?>
  <?php echo HTML::script('js/angular-animate.min.js'); ?>
  <?php echo HTML::script('app/routes.js'); ?>

  <script>
    angular.module('app').constant("CSRF_TOKEN", '<?php echo csrf_token();?>');
  </script>
  <!-- Resource -->
  <?php echo HTML::script('app/resource/notify.js'); ?>

  <!-- Services -->
  <?php echo HTML::script('app/services/userService.js'); ?>
  <?php echo HTML::script('app/services/authService.js'); ?>
  <?php echo HTML::script('app/services/postService.js'); ?>
  <?php echo HTML::script('app/services/profileService.js'); ?>

  <!-- Third party -->
  <?php echo HTML::script('app/third_party/angular-ui-router.js'); ?>
  <?php echo HTML::script('app/third_party/satellizer.min.js'); ?>
  <?php echo HTML::script('app/third_party/angular-contenteditable.js'); ?>
  <?php echo HTML::script('app/third_party/ng-simplePagination.js'); ?>

  <!-- Filters -->
  <?php echo HTML::script('app/filters/filters.js'); ?>


  <!-- Angular controllers -->
  <?php echo HTML::script('app/controllers/mainController.js'); ?>
  <?php echo HTML::script('app/controllers/loginController.js'); ?>
  <?php echo HTML::script('app/controllers/signupController.js'); ?>
  <?php echo HTML::script('app/controllers/myjournalController.js'); ?>


  <?php echo HTML::script('js/bootstrap.min.js'); ?>
  <?php echo HTML::script('js/bootstrap-growl.min.js'); ?>


</body>
</html>