<style>
    .first-row.ng-enter, .wallpost.ng-enter {
        transition:1s;
        opacity: 0;
    }
    .first-row.ng-enter-active, .wallpost.ng-enter-active {
        opacity: 1;
    }
    /*.wallpost.ng-leave {*/
        /*transition: 0.2s;*/
        /*opacity: 1;*/
    /*}*/
    .first-row.ng-enter.ng-leave-active, .wallpost.ng-leave-active {
        opacity: 0;
    }
</style>

<!--Card with Media-->    

<div class="container-fluid">
    <div class="row">
        <div class="alert alert-info text-center">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          For God so loved the world that he gave his one and only Son, that whoever believes in him shall not perish but have eternal life.
          <br>
          <!-- <div class="pull-right"> -->
            <em>- John 3:16</em>
          <!-- </div> -->
        </div>
        <div class="first-row col-md-5 col-xs-12">
            <div class="media block-update-card">
              <a class="pull-left" href="#">
                <img class="media-object update-card-MDimentions" ng-src="{{profile.profile_img}}" alt="...">
              </a>
              <div class="media-body update-card-body">
                <h4 class="media-heading">{{user.firstname}} {{user.lastname}}</h4>
                <p>{{profile.quote}}</p>
              </div>
            </div>

            <div class="block-update-card status">
              <div class="update-card-body">
                <div class="update-card-body">
                  <h4 contenteditable="true" ng-model="newPost.title">Title</h4>
                  <label id="textarea-label">
                    <textarea ng-model="newPost.body" class="form-control" rows="3"></textarea>
                  </label>
                </div>
              </div>
                <div class="row">
                <div class="col-md-6 col-xs-12">

                  <div class="card-action-pellet btn-toolbar pull-left" role="toolbar">
                    <div class="btn-group fa fa-share"></div>
                    <div class="btn-group fa fa-book"></div>
                    <div class="btn-group fa fa-photo"></div>
                  </div>
                </div>

                <div class="col-md-5 col-md-offset-1">
                  <div class="pull-right">
                      <a class="dropdown-toggle btn btn-default btn-sm" data-toggle="dropdown" href="#">
                      Public
                        <span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Friends</a></li>
                        <li><a href="#">Only me</a></li>
                      </ul>
                    <button ng-click="create(newPost)" class="btn btn-info btn-sm">Post</button>
                  </div>
                </div>
              </div>

          </div>
        </div>

        <div class="second-row col-md-6 col-xs-12">
            <div ng-repeat="post in posts | orderBy:'id':true | startFrom:currentPage*pageSize |  limitTo:pageSize" class="width-hundred wallpost">
                <div ng-init="lastIndex($index)" class="block-update-card status">
                    <div class="update-card-body">
                        <div class="update-card-body">
                            <div class="clearfix"></div>
                            <div class="pull-right dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Report</a></li>
                                </ul>
                            </div>
                            <div class="row text-center">
                                <div class="col-md-2 col-md-offset-5">
                                    <img src="/users/images/david-cartoonize.png" id="user-profile-thumbnail" class="img-responsive img-thumbnail">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-md-offset-5 hidden-xs">
                                    <h4>{{author.firstname}} {{author.lastname}}</h4>
                                </div>
                                <div class="col-md-5 col-md-offset-5 visible-xs text-center">
                                    <h4>{{author.firstname}} {{author.lastname}}</h4>
                                </div>
                            </div>

                            <a href="" data-toggle="modal" data-target="#myModal">
                                <!--<img src="assets/img/autumn.jpg" class="img-responsive img-thumbnail">-->
                            </a>
                            <h4 class="text-center">{{post.title}}</h4>
                            <p class="text-center">{{post.created_at | date:'medium'}}</p>
                            <p >{{post.body}}</p>
                        </div>
                    </div>
                    <div class="card-action-pellet btn-toolbar pull-right" role="toolbar">
                        <div class="btn-group fa fa-share" title="Share"></div>
                        <div class="btn-group fa fa-comment" title="Comment"></div>
                        <div class="btn-group fa fa-thumbs-o-up" title="Like"></div>
                        <div class="btn-group fa fa-bookmark" title="Bookmark"></div>
                    </div>
                </div>
            </div><!--END-->

                <button ng-disabled="currentPage == 0" ng-click="currentPage=currentPage-1" class="btn btn-lg btn-primary fa fa-arrow-left col-md-5 col-md-offset-1 col-xs-5 col-xs-offset-1"></button>
                <button ng-disabled="currentPage >= posts.length/pageSize - 1" ng-click="currentPage=currentPage+1"
                        class="btn btn-lg btn-primary fa fa-arrow-right col-md-5 col-xs-5"></button>
        </div>


        </div>
    </div>
</div>

