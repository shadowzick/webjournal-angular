//Bootstrap growl notify function
var Shadowzick = {};
Shadowzick.notify = function(message, type, icon) {
    $.growl(message,{
        icon: 'glyphicon glyphicon-' + icon,
        animate: {
          enter: 'animated rubberBand',
          exit: 'animated bounceOutRight'
        },             
        type: type,
        offset:20,
        delay: 3000,
        placement: {
          from: "top",
          align: "right"
        },
    });
}


