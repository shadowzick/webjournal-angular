var gulp = require('gulp');
var sass = require('gulp-sass');
var gulputil = require('gulp-util');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var autoprefixer = require('gulp-autoprefixer');

// gulp.task('css', function() {
//     gulp.src('app/assets/sass/main.scss')
//         .pipe(sass())
//         .pipe(autoprefixer('last 10 version'))
//         .pipe(gulp.dest('public/css'))
// });

// Run this first for dependencies

gulp.task('scripts', function() {
    gulp.src('public/js/*.js')
        .pipe(concat('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/minified'))
});

// Secondly run this

// gulp.task('scripts', function() {
//     gulp.src('public/app/**/*.js')
//         .pipe(concat('mainScript.min.js'))
//         .pipe(uglify())
//         .pipe(gulp.dest('public/minified'))
// });

//Watcher
gulp.task('watch', function() {
    // gulp.watch('app/assets/sass/**/*.scss', ['css']);
    gulp.watch('public/**/*.js', ['scripts']);
});

gulp.task('default', ['watch']);
